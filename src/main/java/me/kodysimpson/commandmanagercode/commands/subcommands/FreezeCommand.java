package me.kodysimpson.commandmanagercode.commands.subcommands;

import me.kodysimpson.commandmanagercode.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

public class FreezeCommand extends SubCommand {

    @Override
    public String getName() {
        return "freeze";
    }

    @Override
    public String getDescription() {
        return "Turn a player into a block of ice.";
    }

    @Override
    public String getSyntax() {
        return "/prank lock <player>";
    }

    @Override
    public void perform(Player player, String[] args) { // /prank freeze <player>

        Player target = Bukkit.getPlayer(args[1]); //Get the player from the argument provided

        player.sendMessage("You have just frozen " + target.getDisplayName());

        target.setWalkSpeed(0);

        target.sendMessage("GET FROZEN FOOL");

    }

    @Override
    public List<String> getSubcommandArguments(Player player, String[] args) {
        return null;
    }

}
