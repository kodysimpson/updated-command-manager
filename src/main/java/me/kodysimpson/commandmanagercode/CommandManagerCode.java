package me.kodysimpson.commandmanagercode;

import me.kodysimpson.commandmanagercode.commands.CommandManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class CommandManagerCode extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic

        getCommand("prank").setExecutor(new CommandManager());

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
